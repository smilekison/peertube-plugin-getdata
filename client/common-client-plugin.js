function register({ registerHook, peertubeHelpers }) {
  registerHook({
    target: 'action:video-watch.video.loaded',
    handler: () => {
      peertubeHelpers.translate('User name')
      .then(translation => console.log('Translated User name by ' + translation))
    }
    // handler: ({ user }) => {
    //   const totalLikes = [];
    //   // fetch('http://localhost:9000/api/v1/accounts/root/videos')
    //   fetch('https://terakoya-backstage.com/api/v1/accounts/root/videos')
    //     .then((res) => res.json())
    //     .then((data) => {
    //       data.data.map((value) => {
    //         console.log("This is the mapped value:  ", value.likes);
    //         totalLikes.push(value.likes);
    //       })
    //       const sum = totalLikes.reduce((partial_sum, a) => partial_sum + a, 0);
    //       console.log("This is total likes: ", sum)
    //     }).
    //     // .then(function (response) {
    //     // return response.json()
    //     then(function (data) {
    //       console.log(data)
    //     })
    // }
  })

}

export {
  register
}
